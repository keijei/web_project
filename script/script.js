jQuery(document).ready(function () {
  //ここをクリックボタンクリックでリストが開く
  $(".slide_list dt").click(function () {
    $(".slide_list dd").slideToggle("fast");
  });

  //スクロールしたとき「ページトップへ」ボタン表示
  $(window).scroll(function () {
    $(".goto_page_top").show("slow");
  });

  //リスト表示ボタンとボックス表示ボタンで切り替え
  $("#change_list").click(function () {
    $("#img_grp").attr("class", "img_list");
  });
  $("#change_box").click(function () {
    $("#img_grp").attr("class", "img_box");
  });

  //マウスホバーで画像を光らせる
  $('#img_grp img').hover(
    function () {
      $(this).fadeTo(0, 0.6).fadeTo('normal', 1.0);
    },
    function () {
      $(this).fadeTo('fast', 1.0);
    }
  );

  //セレクトボックスチェンジで画像を変更
  let imgNum; //セレクトボックスの番号を格納
  let fileName; //画像ファイル名を格納
  $(".select_img select").change(function () {
    imgNum = $(".select_img select").val();
    fileName = "images/" + imgNum + ".jpg";
    //window.alert(fileName);
    $(".select_img img").attr("src", fileName);
  });
});